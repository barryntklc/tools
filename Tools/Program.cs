﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tools
{
    class Program
    {
        [STAThreadAttribute]
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            //todo create console menu
            //todo add logging

            Run_PrintInfo();
        }

        //todo implement Run_Diff() - compare files in two path roots

        //todo implement Run_AutoLogin() - lets the user set or remove autologin
        //display status
        //set
        //username
        //password
        //domain
        //prompt Changes will not be applied until system restart
        //prompt do you want to restart now?
        //(if set) remove

        static void Run_PrintInfo()
        {
            //todo get computer name
            //todo get network information

            //todo get users
            //todo get user folders and sizes

            //
            //todo move to separate method and return a DataTable
            DataTable Programs = new DataTable();
            Programs.Columns.Add("Name", typeof(String));
            Programs.Columns.Add("Version", typeof(String));
            Programs.Columns.Add("Publisher", typeof(String));
            Programs.Columns.Add("Installed", typeof(String));
            //output as html, csv, json, xlsx

            //https://stackoverflow.com/questions/908850/get-installed-applications-in-a-system
            //https://stackoverflow.com/questions/15524161/c-how-to-get-installing-programs-exactly-like-in-control-panel-programs-and-fe
            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
            {
                foreach (string subkey_name in key.GetSubKeyNames())
                {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                    {
                        try
                        {
                            String DisplayName = subkey.GetValue("DisplayName").ToString();
                            String DisplayVersion = subkey.GetValue("DisplayVersion").ToString();
                            String Publisher = subkey.GetValue("Publisher").ToString();
                            String InstallDate = subkey.GetValue("InstallDate").ToString().Insert(6, "-").Insert(4, "-");
                            var systemComponent = subkey.GetValue("SystemComponent");

                            if (systemComponent == null)
                            {
                                Programs.Rows.Add(DisplayName, DisplayVersion, Publisher, InstallDate);
                            }
                        }
                        catch (Exception e)
                        {
                            //TODO suppress warnings switch
                            //Console.WriteLine("[WARNING] Skipped, NullReferenceException");
                        }


                    }

                }
            }



            //using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
            //{
            //    foreach (string subkey_name in key.GetSubKeyNames())
            //    {
            //        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
            //        {
            //            try
            //            {
            //                String DisplayName = subkey.GetValue("DisplayName").ToString();
            //                String DisplayVersion = subkey.GetValue("DisplayVersion").ToString();
            //                String Publisher = subkey.GetValue("Publisher").ToString();

            //                Console.WriteLine(DisplayName + "\t" + "\t" + DisplayVersion + "\t" + "\t" + Publisher);
            //            }
            //            catch (Exception e)
            //            {
            //                //null reference
            //            }


            //            //Console.WriteLine(subkey.GetValue("DisplayName") + "\t" + "\t" + subkey.GetValue("DisplayVersion") + "\t" + "\t" + subkey.GetValue("Publisher"));
            //            //Console.WriteLine(DisplayName + "\t" + "\t" + DisplayVersion + "\t" + "\t" + Publisher);
            //        }

            //    }
            //}
            //Console.WriteLine(Environment.MachineName);
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.FileName = Environment.MachineName + " Program List";
            saveDialog.Filter = "Comma Separated Values (*.csv)|*.csv|" +
                "Microsoft Excel Spreadsheet (*.xlsx)|*.xlsx|" +
                "JSON (*.json)|*.json|" +
                "HTML (*.html)|*.html";
            saveDialog.DefaultExt = "html";
            saveDialog.AddExtension = true;
            saveDialog.ShowDialog();

            String Path = saveDialog.FileName;

            //Console.WriteLine(Path);
            //foreach (DataRow a in Programs.Rows)
            //{
            //    Console.WriteLine(a.ItemArray[0] + "\t" + "\t" + a.ItemArray[1] + "\t" + "\t" + a.ItemArray[2] + "\t" + "\t" + a.ItemArray[3]);

            //}


            //TODO save
            //TODO print saved message
            Console.WriteLine(saveDialog.FilterIndex);
            if (saveDialog.FilterIndex == 1)
            {
                Console.WriteLine("Saving CSV...");
                Save_CSV(Path, Programs);
                Console.WriteLine("Saved file to \"" + Path + "\"");
            }
            else if (saveDialog.FilterIndex == 2)
            {
                Console.WriteLine("Saving XLSX...");
                Console.WriteLine("[ERROR] Not yet implemented.");
            }
            else if (saveDialog.FilterIndex == 3)
            {
                Console.WriteLine("Saving JSON...");
                Console.WriteLine("[ERROR] Not yet implemented.");
            }
            else if (saveDialog.FilterIndex == 4)
            {
                Console.WriteLine("Saving HTML...");
                Console.WriteLine("[ERROR] Not yet implemented.");
            }

            //TODO optional open file
            //System.Diagnostics.Process.Start(Path);

            //Console.ReadLine();
        }

        static void Save_CSV(String Path, DataTable Programs) {

            foreach (DataRow a in Programs.Rows)
            {
                Console.WriteLine(a.ItemArray[0] + "\t" + "\t" + a.ItemArray[1] + "\t" + "\t" + a.ItemArray[2] + "\t" + "\t" + a.ItemArray[3]);

            }

            List<String> LineBuffer = new List<String>();
            foreach (DataRow a in Programs.Rows)
            {
                String CSVLine = "\"" + a.ItemArray[0] + "\",\"" + a.ItemArray[1] + "\",\"" + a.ItemArray[2] + "\",\"" + a.ItemArray[3] + "\"";
                LineBuffer.Add(CSVLine);
                Console.WriteLine(CSVLine);
            }

            //foreach (String b in LineBuffer)
            //{
            //    Console.WriteLine(b);

            //}

            File.WriteAllLines(Path, LineBuffer.ToArray());

        }
    }
}
